import pytest
import logging
from pyspark.sql import SparkSession
import os
import sys
PACKAGE_PARENT = '..'
SCRIPT_DIR = os.path.dirname(os.path.realpath(os.path.join(os.getcwd(), os.path.expanduser(__file__))))
sys.path.append(os.path.normpath(os.path.join(SCRIPT_DIR, PACKAGE_PARENT)))
from main.spark_job import has_header, ingest_csv_to_delta


logging.getLogger().handlers = []
logging.basicConfig(filename='./Logs/test-logs/test.log', level=logging.INFO)
logger = logging.getLogger(__name__)


# Fixture for creating a SparkSession for testing
@pytest.fixture(scope="session")
def spark_session():
    # Create a SparkSession for testing
    logger.info("Creating SparkSession for testing...")
    spark = SparkSession.builder \
        .appName("test_spark_job") \
        .master("local[2]") \
        .config("spark.jars.packages", "io.delta:delta-core_2.12:1.2.1") \
        .config("spark.sql.extensions", "io.delta.sql.DeltaSparkSessionExtension") \
        .config("spark.sql.catalog.spark_catalog", "org.apache.spark.sql.delta.catalog.DeltaCatalog") \
        .getOrCreate()
    yield spark
    spark.stop()
    logger.info("SparkSession stopped.")


# Test case to verify has_header function with CSV file containing a header
def test_has_header_with_header(test_csv_path="data_sources/concrete.csv"):
    logger.info(f"Testing has_header function with file: {test_csv_path}")
    assert has_header(test_csv_path) == True


# Test case to verify has_header function with CSV file without a header
def test_has_header_without_header(test_csv_path="data_sources/insurance.csv"):
    logger.info(f"Testing has_header function with file: {test_csv_path}")
    assert has_header(test_csv_path) == False


# Test case to verify ingest_csv_to_delta function
def test_ingest_csv_to_delta(spark_session, tmpdir, test_csv_path="data_sources/NY-House-Dataset.csv"):
    logger.info(f"Testing ingest_csv_to_delta function with file: {test_csv_path}")
    # Extract the folder path from the test CSV file path
    folder_path = os.path.dirname(test_csv_path)
    
    # Test ingest_csv_to_delta function
    delta_table_path = str(tmpdir / "DeltaLake")
    ingest_csv_to_delta(spark_session, folder_path, delta_table_path)
    
    # Read Delta table into DataFrame
    df_delta = spark_session.read.format("delta").load(delta_table_path)
    
    # Verify DataFrame is not empty
    assert df_delta.count() > 0

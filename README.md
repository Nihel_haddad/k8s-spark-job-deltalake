
# Spark on Kubernetes: DeltaLake Pipeline

Welcome to the Spark DeltaLake Ingestion Project! This project showcases a robust PySpark DeltaLake ingestion job, Docker containerization, and Kubernetes deployment. The job efficiently processes CSV files, enriches the data with extra columns, and appends it atomically to Delta tables. Local testing is facilitated by Docker Compose, while Kubernetes is utilized for seamless production-ready deployment.

## Project Overview

1. **Design and code a Spark Job:**
   - The Spark job ingests 1 or multiple CSV files into DeltaLake.
   - It handles files with and without headers.
   - The job adds 2 extra columns to the output DataFrame:
     - `ingestion_tms`: ingestion timestamp format: YYYY-MM-DD HH:mm:SS.
     - `batch_id`: UUID v4.
   - The job uses the APPEND write mode to atomically add new data to Delta table.

2. **Docker and Kubernetes Configuration:**
   - Docker Compose YAML file is provided to run the job from a container.
   - Dockerfiles are included for Spark Job and SparkHistoryServer containers.

3. **Deployment:**
   - The solution includes a production-ready system design, deployable on Kubernetes

## Getting Started

To get this project up and running on your local machine and deployed on Kubernetes, follow the steps below:

1. **Clone this repository:**

    ```bash
    git clone https://gitlab.com/Nihel_haddad/k8s-spark-job-deltalake.git
    ```

2. **Install dependencies:**

    ```bash
    pip install -r requirements.txt
    ```

## Local Testing with Docker Compose

Simplify your local testing using Docker Compose. Follow these steps:

1. **Build and start Docker containers:**

    ```bash
    sudo docker build -t spark_job -f docker_files/dockerfile_spark_job .
    sudo docker build -t spark_history_server -f docker_files/dockerfile_spark_history_server
    ```

![Image Description](https://gitlab.com/Nihel_haddad/k8s-spark-job-deltalake/raw/main/images/build_images.png)
   
    ```sudo docker-compose up -d
    ```

![Image Description](https://gitlab.com/Nihel_haddad/k8s-spark-job-deltalake/raw/main/images/docker-compose.png)

This command launches the Spark job container and SparkHistoryServer container in detached mode.

2. **Access SparkHistoryServer:**

    Open [http://localhost:18080](http://localhost:18080) to view Spark job logs.

![Image Description](https://gitlab.com/Nihel_haddad/k8s-spark-job-deltalake/raw/main/images/local_testing.png)

## Push Docker Images to Docker Hub

Push the Docker images to Docker Hub for distribution. Follow these steps:

1. **Tag Docker images:**

    ```bash
    sudo docker tag spark_history_server:latest 270519961101/spark_history_server:latest
    sudo docker tag spark_job:latest 270519961101/spark_job:latest
    ```

2. **Push Spark History Server and Spark Job images:**

    ```bash
    sudo docker push 270519961101/spark_history_server:latest
    sudo docker push 270519961101/spark_job:latest
    ```
![Image Description](https://gitlab.com/Nihel_haddad/k8s-spark-job-deltalake/raw/main/images/docker-hub.png)

## Kubernetes Deployment

Deploy this solution to Kubernetes for a robust and scalable environment. Follow these steps:

1. **Install Minikube and kubectl:**

    ```bash
    curl -LO https://storage.googleapis.com/minikube/releases/latest/minikube_latest_amd64.deb
    sudo dpkg -i minikube_latest_amd64.deb
    
    sudo apt-get update && sudo apt-get install -y kubectl
    ```

2. **Start Minikube:**

    ```bash
    minikube start
    ```

3. **Deploy the application to Kubernetes:**

    ```bash
    kubectl apply -f deployment/job_spark-job.yaml
    kubectl apply -f deployment/deploy_job_history_server.yaml
    kubectl apply -f deployment/spark-history-server-volume.yaml
    kubectl apply -f deployment/spark-history-server-pvc.yaml
    kubectl apply -f deployment/service.yaml
    ```

    Deploy the Spark job and SparkHistoryServer along with necessary configurations.

4. **Monitor the deployed resources:**

    ```bash
    kubectl get pods
    kubectl get services
    ```
![Image Description](https://gitlab.com/Nihel_haddad/k8s-spark-job-deltalake/raw/main/images/kubectl_infos.png)

    Check the status of the deployed pods and services.

5. **Access SparkHistoryServer using Minikube:**

    ```bash
    minikube service spark-history-server --url
    ```
![Image Description](https://gitlab.com/Nihel_haddad/k8s-spark-job-deltalake/raw/main/images/run_service.png)

    Open the provided URL to access SparkHistoryServer.

![Image Description](https://gitlab.com/Nihel_haddad/k8s-spark-job-deltalake/raw/main/images/deployment-spark-job-logs.png)

6. **Delete all Kubernetes resources:**

    ```bash
    kubectl delete all --all
    ```

7. **Stop Minikube:**

    ```bash
    minikube stop
    ```

    Clean up all deployed resources in the Kubernetes cluster and stop Minikube.


